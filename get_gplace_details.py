import configparser
import json
import logging
import csv
import time
import warnings

import googlemaps
import pandas as pd
import snowflake.connector
from snowflake.connector.pandas_tools import write_pandas

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_gplaces.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

config = configparser.ConfigParser()
config.read("ConfigFile.properties")
api_key = config.get(
    "Parametros", "api_key"
)


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    ACCOUNT = 'hg51401'
    host = 'hg51401.snowflakecomputing.com'
    USER = 'ALAIN.MORE@RAPPI.COM'
    warehouse = 'ECOMMERCE'
    DATABASE = 'FIVETRAN'
    ROLE = 'GLOBAL_ECOMMERCE_WRITE_ROLE'
    SCHEMA = 'GLOBAL_ECOMMERCE'

    connection = snowflake.connector.connect(
        user=USER,
        account=ACCOUNT,
        authenticator='externalbrowser',
        database=DATABASE,
        role=ROLE,
        schema=SCHEMA
    )
    
    connection.cursor().execute("ALTER SESSION SET QUERY_TAG = 'global-ecommerce-data'")
    connection.cursor().execute("USE ROLE GLOBAL_ECOMMERCE_WRITE_ROLE")
    connection.cursor().execute("USE SCHEMA FIVETRAN.GLOBAL_ECOMMERCE")
    connection.cursor().execute("USE WAREHOUSE ECOMMERCE")

    gmaps = googlemaps.Client(key=api_key)
    my_fields = [
        "name",
        "type",
        "formatted_address",
        "formatted_phone_number",
        "website",
        "business_status",
        "opening_hours",
        "geometry",
        "url",
        "rating",
        "user_ratings_total"
    ]
    results_list = []
    try:
        n = 0
        with open("gplaces_ids.csv", "r") as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                n = n + 1
                result_dict = {}
                _place_id = row["GOOGLE_PLACE_ID"]
                log.info("")
                log.info("Buscando: " + _place_id)
                log.info("")

                time.sleep(1)
                try:
                    place_details = gmaps.place(
                        place_id=_place_id, fields=my_fields)
                    result = place_details["result"]
                    log.info(result)

                    result_dict["PLACE_ID"] = _place_id
                    if "name" in result:
                        result_dict["NAME"] = result["name"]
                    if "types" in result:
                        result_dict["TYPES"] = json.dumps(result["types"])
                    if "formatted_address" in result:
                        result_dict["FORMATTED_ADDRESS"] = result["formatted_address"]
                    if "formatted_phone_number" in result:
                        result_dict["FORMATTED_PHONE_NUMBER"] = result["formatted_phone_number"]
                    if "website" in result:
                        result_dict["WEBSITE"] = result["website"]
                    if "business_status" in result:
                        result_dict["BUSINESS_STATUS"] = result["business_status"]
                    if "opening_hours" in result:
                        result_dict["OPENING_HOURS"] = json.dumps(
                            result["opening_hours"]["weekday_text"]
                        )
                    if "geometry" in result:
                        result_dict["LAT"] = result["geometry"]["location"]["lat"]
                        result_dict["LNG"] = result["geometry"]["location"]["lng"]
                    if "url" in result:
                        result_dict["URL"] = result["url"]
                    if "rating" in result:
                        result_dict["RATING"] = result["rating"]
                    if "user_ratings_total" in result:
                        result_dict["USER_RATINGS_TOTAL"] = result["user_ratings_total"]

                    results_list.append(result_dict)
                    log.info("")

                    if n >= 1000:
                        log.info("")
                        log.info(">>>>>>>> Guardando...")
                        df = pd.DataFrame(results_list)
                        print(df)
                        success, nchunks, nrows, _ = write_pandas(
                            connection, df, 'ANTOJOS_GPLACES')
                        log.info(f'{nrows} added to table, {success}')
                        results_list = []
                        n = 0
                except googlemaps.exceptions.ApiError:
                    log.exception('Error con place id %s', _place_id)
                except:
                    log.exception("Error general: ")

            log.info("")
            if results_list:
                log.info(">>>>>>>> Fin, Guardando restantes...")
                df = pd.DataFrame(results_list)
                print(df)
                success, nchunks, nrows, _ = write_pandas(
                    connection, df, 'ANTOJOS_GPLACES')
                log.info(f'{nrows} added to table', {success})
            else:
                log.info(">>>>>>> Fin, no quedaron restantes...")

    finally:
        connection.close()
        print("Connection cerrada")


if __name__ == "__main__":
    main()
