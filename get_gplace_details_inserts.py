import configparser
import jinja2
import json
import logging
import csv
import warnings

import googlemaps
import pandas as pd
import snowflake.connector
from snowflake.connector.pandas_tools import write_pandas

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_gplaces_inserts.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

config = configparser.ConfigParser()
config.read("ConfigFile.properties")
api_key = config.get(
    "Parametros", "api_key"
)


def insert_place(connection, result_dict):
    params = dict(
        PLACE_ID=result_dict["PLACE_ID"],
        NAME=(result_dict["NAME"]).replace("'", ""),
        TYPES=result_dict["TYPES"],
        FORMATTED_ADDRESS=(result_dict["FORMATTED_ADDRESS"]).replace("'", ""),
        FORMATTED_PHONE_NUMBER=result_dict["FORMATTED_PHONE_NUMBER"],
        WEBSITE=result_dict["WEBSITE"],
        BUSINESS_STATUS=result_dict["BUSINESS_STATUS"],
        OPENING_HOURS=result_dict["OPENING_HOURS"],
        LAT=result_dict["LAT"],
        LNG=result_dict["LNG"],
        URL=result_dict["URL"],
        RATING=result_dict["RATING"],
        USER_RATINGS_TOTAL=result_dict["USER_RATINGS_TOTAL"]
    )

    insert = """
        insert into GLOBAL_ECOMMERCE.ANTOJOS_GPLACES
        (PLACE_ID,NAME,TYPES,FORMATTED_ADDRESS,FORMATTED_PHONE_NUMBER,WEBSITE,BUSINESS_STATUS,OPENING_HOURS,LAT,LNG,
        URL,RATING,USER_RATINGS_TOTAL) 
        values( '{{PLACE_ID}}','{{NAME}}','{{TYPES}}','{{FORMATTED_ADDRESS}}','{{FORMATTED_PHONE_NUMBER}}','{{WEBSITE}}',
        '{{BUSINESS_STATUS}}','{{OPENING_HOURS}}',{{LAT}},{{LNG}},'{{URL}}',{{RATING}},{{USER_RATINGS_TOTAL}});
    """
    template = jinja2.Environment(
        loader=jinja2.BaseLoader).from_string(insert)
    formatted_template = template.render(**params)
    log.info(formatted_template)
    try:
        df_insert = pd.read_sql_query(
            con=connection, sql=formatted_template)
        log.info(df_insert)
    except Exception as ex:
        log.error(ex)


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    ACCOUNT = 'hg51401'
    USER = 'ALAIN.MORE@RAPPI.COM'
    DATABASE = 'FIVETRAN'
    ROLE = 'GLOBAL_ECOMMERCE_WRITE_ROLE'
    SCHEMA = 'GLOBAL_ECOMMERCE'

    connection = snowflake.connector.connect(
        user=USER,
        account=ACCOUNT,
        authenticator='externalbrowser',
        database=DATABASE,
        role=ROLE,
        schema=SCHEMA
    )

    connection.cursor().execute("USE ROLE GLOBAL_ECOMMERCE_WRITE_ROLE")
    connection.cursor().execute("USE SCHEMA FIVETRAN.GLOBAL_ECOMMERCE")
    connection.cursor().execute("USE WAREHOUSE ECOMMERCE")

    gmaps = googlemaps.Client(key=api_key)
    my_fields = [
        "name",
        "type",
        "formatted_address",
        "formatted_phone_number",
        "website",
        "business_status",
        "opening_hours",
        "geometry",
        "url",
        "rating",
        "user_ratings_total"
    ]
    try:
        with open("gplaces_ids.csv", "r") as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                result_dict = {}
                _place_id = row["GOOGLE_PLACE_ID"]
                log.info("")
                log.info("Buscando: " + _place_id)
                log.info("")

                try:
                    place_details = gmaps.place(
                        place_id=_place_id, fields=my_fields)
                    result = place_details["result"]
                    log.info(result)

                    result_dict["PLACE_ID"] = _place_id
                    if "name" in result:
                        result_dict["NAME"] = result["name"]
                    if "types" in result:
                        result_dict["TYPES"] = json.dumps(result["types"])
                    if "formatted_address" in result:
                        result_dict["FORMATTED_ADDRESS"] = result["formatted_address"]
                    if "formatted_phone_number" in result:
                        result_dict["FORMATTED_PHONE_NUMBER"] = result["formatted_phone_number"]
                    if "website" in result:
                        result_dict["WEBSITE"] = result["website"]
                    if "business_status" in result:
                        result_dict["BUSINESS_STATUS"] = result["business_status"]
                    if "opening_hours" in result:
                        result_dict["OPENING_HOURS"] = json.dumps(
                            result["opening_hours"]["weekday_text"]
                        )
                    if "geometry" in result:
                        result_dict["LAT"] = result["geometry"]["location"]["lat"]
                        result_dict["LNG"] = result["geometry"]["location"]["lng"]
                    if "url" in result:
                        result_dict["URL"] = result["url"]
                    if "rating" in result:
                        result_dict["RATING"] = result["rating"]
                    if "user_ratings_total" in result:
                        result_dict["USER_RATINGS_TOTAL"] = result["user_ratings_total"]

                    insert_place(connection, result_dict)

                except googlemaps.exceptions.ApiError:
                    log.exception('Error con place id %s', _place_id)
                except:
                    log.exception("Error general: ")

            log.info("")

    finally:
        connection.close()
        print("Connection cerrada")


if __name__ == "__main__":
    main()
